

namespace models
{
    public class TransactionDTO
    {
        public string Destination { get; set; }
        public decimal Amount { get; set; }
        public double Fee { get; set; }
    }
}