﻿using System;
using System.Net;

namespace models.Response
{
    public class ResponseBase
    { 
        public int Code { get  ; set; }
        public Exception Exception { get; set; }
        public string Message{ get; set; }
        public HttpStatusCode HttpStatus { get; set; }
    }
}
