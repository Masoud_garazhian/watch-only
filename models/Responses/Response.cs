﻿using System;

namespace models.Response
{
    public class Response<T> : ResponseBase
    {
        public T Result { get; set; }

    }
}
