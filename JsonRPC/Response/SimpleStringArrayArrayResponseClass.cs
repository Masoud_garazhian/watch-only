﻿


using System.Runtime.Serialization;

namespace JsonRPC.Response.Model
{
    [DataContract]
    public class SimpleStringArrayArrayResponseClass : AbstractResponseClass
    {
        [DataMember]
        public string[][] result { get; set; }
    }
}
