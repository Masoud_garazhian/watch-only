﻿


using System.Runtime.Serialization;

namespace JsonRPC.Response.Model
{
    [DataContract]
    public class SimpleStringResponseClass : AbstractResponseClass
    {
        [DataMember]
        public string result { get; set; }

        public override string ToString()
        {
            if (this.error != null && this.error?.code != 0)
                return this.error.message;
            return this.result;
        }
    }
}
