﻿


using System.Runtime.Serialization;

namespace JsonRPC.Response.Model
{
    [DataContract]
    public class SimpleStringArrayResponseClass : AbstractResponseClass
    {
        [DataMember]
        public string[] result { get; set; }
    }
}
