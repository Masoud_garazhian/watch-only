

using JsonRPC.Request;
using JsonRPC.Request.Methods;
using JsonRPC.Request.Methods.Address;
using JsonRPC.Request.Methods.Payment;
using JsonRPC.Response.Model;

namespace JsonRPC
{
    public interface IElectrum_JSONRPC_Client
    {

        SimpleStringArrayArrayResponseClass GetExListWalletAddresses(bool? filter_receiving = null, bool? filter_change = null, bool? filter_frozen = null, bool? filter_unused = null, bool? filter_funded = null);
        SimpleStringArrayResponseClass GetListWalletAddresses(bool? filter_receiving = null, bool? filter_change = null, bool? filter_frozen = null, bool? filter_unused = null, bool? filter_funded = null);
        AddressHistoryResponseClass GetAddressHistory(string address);
        string CreateNewAddress();
        string LoadWallet();
        BalanceResponseClass GetBalanceWallet();
        CreatePaymentResponseClass CreatePaymentRequest(double amount, long? expiration = null, bool? force = null, string memo = null);
        WalletTransactionsHistoryResponseClass GetTransactionsHistoryWallet(bool? show_addresses = null, bool? show_fiat = null, bool show_fees = false, int? year = null, long? from_height = null, long? to_height = null);
        GetTransactionResponseClass GetTransaction(string txid);
        BalanceResponseClass GetAddressBalance(string address);
        SimpleBoolResponseClass ValidateAddress(string address);
        SimpleStringResponseClass GetElectrumVersion();
        SimpleBoolResponseClass IsAddressMine(string check_address);
    }
}