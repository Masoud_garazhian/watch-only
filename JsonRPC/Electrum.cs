using System;
using System.Collections.Generic;
using System.Linq;
using models;
using models.Response;

namespace JsonRPC
{
    public class Electrum : IElectrum
    {
        private readonly Electrum_JSONRPC_Client electrum_JRPC;

        public Electrum()
        {
            this.electrum_JRPC = new Electrum_JSONRPC_Client("masoud", "garazhian", "http://127.0.0.1", 7777); //TODO: secure passwords
        }

        public Response<string> GetAddress()
        {
            try
            {
                return new Response<string>() { Result = electrum_JRPC.CreateNewAddress(), HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10001);
            }
        }

        public Response<string> GetBalance()
        {
            try
            {
                var balanceResult = electrum_JRPC.GetBalanceWallet();

                return new Response<string>() { Result = $"confirmed: {balanceResult.result.confirmed}  unconfirmed: {balanceResult.result.unconfirmed}", HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10002);
            }
        }

        public Response<List<string>> GetExpandedAddresses()
        {
            try
            {
                var addressList = electrum_JRPC.GetExListWalletAddresses().result.Select(a => a[0].ToString() + " " + a[1].ToString() + " " + a[2].ToString()).ToList();
                return new Response<List<string>>() { Result = addressList, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<List<string>>(ex, 10003);
            }
        }

        public Response<List<string>> GetAddresses()
        {
            try
            {
                var addressList = electrum_JRPC.GetListWalletAddresses().result.Select(a => a.ToString()).ToList();
                return new Response<List<string>>() { Result = addressList, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<List<string>>(ex, 10004);
            }
        }


        public Response<string> BroadcastTransaction(SignedTx tx)
        {
            try
            {
                var txResult = electrum_JRPC.BroadcastTransaction(tx.Tx);
                return new Response<string>() { Result = txResult, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10006);
            }
        }

        public Response<string> GetFeeRate()
        {
            try
            {
                var feeResult = electrum_JRPC.GetFeeRate();
                return new Response<string>() { Result = feeResult, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10007);
            }
        }

        public Response<bool> ValidateAddress(string address)
        {
            try
            {
                var result = electrum_JRPC.ValidateAddress(address);
                return new Response<bool>() { Result = result.result ?? false, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<bool>(ex, 10007);
            }
        }


        public Response<string> CreateUnsignedTransaction(TransactionDTO transaction)
        {
            try
            {
                var utx = electrum_JRPC.CreateUnsignedTransaction(transaction);
                return new Response<string>() { Result = utx, HttpStatus = System.Net.HttpStatusCode.OK };
            }
            catch (System.Exception ex)
            {
                return ReturnError<string>(ex, 10009);
            }
        }

        private static Response<T> ReturnError<T>(Exception ex, int code, System.Net.HttpStatusCode httpStatus = System.Net.HttpStatusCode.InternalServerError)
        {
            return new Response<T>() { Result = default(T), Code = code, Exception = ex, HttpStatus = httpStatus };
        }
    }
}