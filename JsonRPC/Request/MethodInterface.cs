﻿ using System.Collections.Specialized;

namespace JsonRPC.Request
{
    public interface MethodInterface
    {
        object execute();
    }
}
