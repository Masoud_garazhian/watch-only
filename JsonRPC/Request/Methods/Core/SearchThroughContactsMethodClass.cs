﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary> 
    /// Search through contacts, return matching entries
    /// </summary>
    class SearchThroughContactsMethodClass : AbstractMethodClass // searchcontacts(self, query):
    {
        public override string method => "searchcontacts";

        [Required]
        public string query;

        public SearchThroughContactsMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(query))
                throw new ArgumentNullException("query");

            options.Add("query", query);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
