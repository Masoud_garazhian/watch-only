﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary> 
    /// Set a configuration variable. 'value' may be a string or a Python expression
    /// </summary>
    class SetConfigValueMethodClass : AbstractMethodClass // commands.py signature setconfig(self, key, value):
    {
        public override string method => "setconfig";

        /// <summary>
        /// Variable name
        /// </summary>
        [Required]
        public string key;

        [Required]
        public string value;

        public SetConfigValueMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }
        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");
            if (string.IsNullOrWhiteSpace(value))
                throw new ArgumentNullException("value");

            options.Add("key", key);
            options.Add("value", value);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
