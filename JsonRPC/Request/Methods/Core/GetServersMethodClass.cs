﻿using System;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary> 
    /// Return the list of available servers
    /// </summary>
    class GetServersMethodClass : AbstractMethodClass
    {
        public override string method => "getservers";

        public GetServersMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
