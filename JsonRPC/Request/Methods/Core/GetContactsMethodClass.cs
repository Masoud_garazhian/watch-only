﻿using System;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary>
    /// Show your list of contacts
    /// </summary>
    public class GetContactsMethodClass : AbstractMethodClass
    {
        public override string method => "listcontacts";

        public GetContactsMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);

            // return deserialized object //Masoud
        }
    }
}
