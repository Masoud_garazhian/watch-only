﻿




using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary>
    /// Return a configuration variable
    /// </summary>
    class GetConfigMethodClass : AbstractMethodClass // commands.py signature getconfig(self, key):
    {
        public override string method => "getconfig";

        /// <summary>
        /// Variable name
        /// </summary>
        [Required]
        public string key;

        public GetConfigMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");

            options.Add("key", key);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
