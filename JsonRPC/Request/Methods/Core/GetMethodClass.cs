﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary>
    /// Return a configuration variable.
    /// </summary>
    class GetMethodClass : AbstractMethodClass // commands.py signature get(self, key):
    {
        public override string method => "get";

        /// <summary>
        /// Variable name
        /// </summary>
        [Required]
        public string key;
        public GetMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");

            options.Add("key", key);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
