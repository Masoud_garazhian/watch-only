﻿using System;

namespace JsonRPC.Request.Methods.Core
{
    /// <summary>
    /// List of commands
    /// </summary>
    class GetCommandsMethodClass : AbstractMethodClass // commands.py signature commands(self):
    {
        public override string method => "commands";
        public GetCommandsMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
             return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
