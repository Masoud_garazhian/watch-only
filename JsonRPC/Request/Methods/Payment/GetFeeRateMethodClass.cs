﻿




using System;
using JsonRPC.Response.Model;
using Newtonsoft.Json;

namespace JsonRPC.Request.Methods.Payment
{
    /// <summary> 
    /// Return current suggested fee rate (in sat/kvByte), according to config settings or supplied parameters
    /// </summary>
    class GetFeeRateMethodClass : AbstractMethodClass // commands.py signature getfeerate(self, fee_method=None, fee_level=None):
    {
        public enum fee_methods { @static, eta, mempool };

        public override string method => "getfeerate";
        public fee_methods? fee_method = null;
        public double? fee_level = null;

        public GetFeeRateMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (fee_method != null)
                options.Add("fee_method", fee_method.ToString());

            string separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            if (fee_level != null)
                options.Add("fee_level", fee_level.ToString().Replace(".", separator).Replace(",", separator));

            string jsonrpc_raw_data = Client.Execute(method, options);
            return JsonConvert.DeserializeObject<SimpleStringResponseClass>(jsonrpc_raw_data.ToString());
            // return deserialized object //Masoud
        }
    }
}
