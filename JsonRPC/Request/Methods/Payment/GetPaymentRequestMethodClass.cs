﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Payment
{
    /// <summary>
    /// Return a payment request.
    /// </summary>
    class GetPaymentRequestMethodClass : AbstractMethodClass // commands.py signature getrequest(self, key):
    {
        public override string method => "getrequest";

        /// <summary>
        /// (in version 3.3.8 take by BTC address)
        /// </summary>
        [Required]
        public string key;

        public GetPaymentRequestMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(key))
                throw new ArgumentNullException("key");

            options.Add("key", key);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
