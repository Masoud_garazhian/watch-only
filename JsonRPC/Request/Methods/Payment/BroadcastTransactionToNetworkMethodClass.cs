﻿




using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Payment
{
    /// <summary> 
    /// Broadcast a transaction to the network
    /// </summary>
    class BroadcastTransactionToNetworkMethodClass : AbstractMethodClass // commands.py signature broadcast(self, tx):
    {
        public override string method => "broadcast";

        /// <summary>
        /// Serialized transaction (hexadecimal)
        /// </summary>
        [Required]
        public string tx;

        public BroadcastTransactionToNetworkMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(tx))
                throw new ArgumentNullException("tx");

            options.Add("tx", tx);

            return Client.Execute(method, options); 
            // return deserialized object //Masoud
        }
    }
}
