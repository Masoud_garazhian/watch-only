﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Payment
{
    /// <summary> 
    /// Remove a payment request
    /// </summary>
    class RemovePaymentRequestMethodClass : AbstractMethodClass // commands.py signature rmrequest(self, address):
    {
        public override string method => "rmrequest";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        public RemovePaymentRequestMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
