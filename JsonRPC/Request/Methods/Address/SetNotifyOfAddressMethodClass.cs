﻿




using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Address
{
    /// <summary> 
    /// Watch an address. Every time the address changes, a http POST is sent to the URL.
    /// </summary>
    public class SetNotifyOfAddressMethodClass : AbstractMethodClass // commands.py signature notify(self, address: str, URL: str):
    {
        public override string method => "notify";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        [Required]
        public string URL;

        public SetNotifyOfAddressMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            if (string.IsNullOrWhiteSpace(URL))
                throw new ArgumentNullException("URL");

            options.Add("address", address);
            options.Add("URL", URL);
            return  Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
