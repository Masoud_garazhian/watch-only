﻿




using JsonRPC.Response.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Address
{
    /// <summary> 
    /// Return the transaction history of any address. Note: This is a walletless server query, results are not checked by SPV.
    /// </summary>
    class GetAddressHistoryMethodClass : AbstractMethodClass // commands.py signature getaddresshistory(self, address):
    {
        public override string method => "getaddresshistory";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        public GetAddressHistoryMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);
            string jsonrpc_raw_data = Client.Execute(method, options);
            AddressHistoryResponseClass result = new AddressHistoryResponseClass();
            return result.ReadObject(jsonrpc_raw_data);
        }
    }
}
