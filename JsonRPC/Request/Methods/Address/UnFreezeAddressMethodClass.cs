﻿




using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Address
{
    /// <summary> 
    /// Unfreeze address. Unfreeze the funds at one of your wallet's addresses
    /// </summary>
    public class UnFreezeAddressMethodClass : AbstractMethodClass // commands.py signature unfreeze(self, address):
    {
        public override string method => "unfreeze";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        public UnFreezeAddressMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
