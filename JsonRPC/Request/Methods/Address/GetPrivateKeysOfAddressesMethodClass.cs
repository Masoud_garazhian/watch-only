﻿ 
using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Address
{
    /// <summary> 
    /// Get private keys of addresses. You may pass a single wallet address, or a list of wallet addresses
    /// </summary>
    class GetPrivateKeysOfAddressesMethodClass : AbstractMethodClass // commands.py signature getprivatekeys(self, address, password=None):
    {
        public override string method => "getprivatekeys";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        public string password = null;

        public GetPrivateKeysOfAddressesMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);

            if (!string.IsNullOrEmpty(password))
                options.Add("password", password);

            return Client.Execute(method, options);
            return jsonrpc_raw_data;
            // return deserialized object //Masoud
        }
    }
}
