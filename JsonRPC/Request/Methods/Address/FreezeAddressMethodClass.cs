﻿




using JsonRPC.Response.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Address
{
    /// <summary> 
    /// Freeze address. Freeze the funds at one of your wallet's addresses
    /// </summary>
    public class FreezeAddressMethodClass : AbstractMethodClass // commands.py signature freeze(self, address):
    {
        public override string method => "freeze";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        public FreezeAddressMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);
            string jsonrpc_raw_data = Client.Execute(method, options);
            SimpleBoolResponseClass result = new SimpleBoolResponseClass();
            return result.ReadObject(jsonrpc_raw_data);
        }
    }
}
