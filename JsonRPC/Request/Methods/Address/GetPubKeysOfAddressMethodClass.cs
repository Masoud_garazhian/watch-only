﻿




using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Address
{
    /// <summary> 
    /// Return the public keys for a wallet address
    /// </summary>
    public class GetPubKeysOfAddressMethodClass : AbstractMethodClass // commands.py signature getpubkeys(self, address):
    {
        public override string method => "getpubkeys";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        public GetPubKeysOfAddressMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);
            return Client.Execute(method, options);
            
            // return deserialized object //Masoud
        }
    }
}
