﻿




using JsonRPC.Response.Model;
using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Address
{
    /// <summary> 
    /// Check if address is in wallet. Return true if and only address is in wallet
    /// </summary>
    public class IsAddressMineMethodClass : AbstractMethodClass // commands.py signature ismine(self, address):
    {
        public override string method => "ismine";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;

        public IsAddressMineMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);
            return Client.Execute(method, options);
            SimpleBoolResponseClass result = new SimpleBoolResponseClass();
            return result.ReadObject(jsonrpc_raw_data);
        }
    }
}