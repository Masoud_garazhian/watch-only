﻿




namespace JsonRPC.Request.Methods
{
    /// <summary>
    /// Return the version of Electrum.
    /// </summary>
    public class VersionMethodClass : AbstractMethodClass
    {
        public override string method => "version";

        public VersionMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);
            Response.Model.SimpleStringResponseClass result = new Response.Model.SimpleStringResponseClass();
            return result.ReadObject(jsonrpc_raw_data);
        }
    }
}
