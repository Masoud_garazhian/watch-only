﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary>
    /// Create multisig address
    /// </summary>
    class CreateMultisigAddressMethodClass : AbstractMethodClass // commands.py signature createmultisig(self, num, pubkeys):
    {
        public override string method => "createmultisig";

        [Required]
        public int num;

        [Required]
        public string pubkeys;

        public CreateMultisigAddressMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (num <= 0)
                throw new ArgumentException("cant be null", "num");
            if (string.IsNullOrWhiteSpace(pubkeys))
                throw new ArgumentNullException("pubkeys");

            options.Add("num", num.ToString());
            options.Add("pubkeys", pubkeys);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
