﻿using System;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary> 
    /// Returns the first unused address of the wallet, or None if all addresses are used. An address is considered as used if it has received a transaction, or if it is used in a payment request
    /// </summary>
    class GetUnusedAddressMethodClass : AbstractMethodClass
    {
        public override string method => "getunusedaddress";

        public GetUnusedAddressMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            return Client.Execute(method, options);

            // return deserialized object //Masoud
        }
    }
}
