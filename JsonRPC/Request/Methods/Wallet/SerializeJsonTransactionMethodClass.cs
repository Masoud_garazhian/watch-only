﻿




using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary>
    /// Создать транзакцию из json входов. Входы должны иметь redeemPubkey. Выходы должны быть список {'address':address, 'value':satoshi_amount}
    /// ~ ~ ~
    /// Create a transaction from json inputs. Inputs must have a redeemPubkey. Outputs must be a list of {'address':address, 'value':satoshi_amount}
    /// </summary>
    class SerializeJsonTransactionMethodClass : AbstractMethodClass // commands.py signature serialize(self, jsontx):
    {
        public override string method => "serialize";

        [Required]
        public string jsontx;

        public SerializeJsonTransactionMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(jsontx))
                throw new ArgumentNullException("jsontx");

            options.Add("jsontx", jsontx);
            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
