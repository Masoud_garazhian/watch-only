﻿




using System;
using System.ComponentModel.DataAnnotations;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary>
    /// Подписать платежный запрос OpenAlias
    /// ~ ~ ~
    /// Sign payment request with an OpenAlias
    /// </summary>
    class SignPaymentRequestMethodClass : AbstractMethodClass // commands.py signature signrequest(self, address, password=None):
    {
        public override string method => "signrequest";

        /// <summary>
        /// Bitcoin address
        /// </summary>
        [Required]
        public string address;
        public string password = null;

        public SignPaymentRequestMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentNullException("address");

            options.Add("address", address);

            if (!string.IsNullOrEmpty(password))
                options.Add("password", password);

            return Client.Execute(method, options);
            // return deserialized object //Masoud
        }
    }
}
