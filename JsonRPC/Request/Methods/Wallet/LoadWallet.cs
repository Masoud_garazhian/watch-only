




using JsonRPC.Response.Model;

namespace JsonRPC.Request.Methods.Wallet
{
    /// <summary>
    /// Create a new receiving address, beyond the gap limit of the wallet
    /// </summary>
    class LoadWalletMethodClass : AbstractMethodClass
    {
        public override string method => "load_wallet";

        public LoadWalletMethodClass(Electrum_JSONRPC_Client client) : base(client)
        {

        }

        public override object execute()
        {
            string jsonrpc_raw_data = Client.Execute(method, options);
            SimpleStringResponseClass result = new SimpleStringResponseClass();
            return result.ReadObject(jsonrpc_raw_data);
        }
    }
}
