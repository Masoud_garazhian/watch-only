using System.Collections.Generic;
using models;
using models.Response;

namespace JsonRPC
{
    public interface IElectrum
    {
        Response<string> GetAddress();
        Response<string> GetBalance();
        Response<List<string>> GetExpandedAddresses();
        Response<List<string>> GetAddresses(); 
        Response<string> BroadcastTransaction(SignedTx tx);
        Response<bool> ValidateAddress(string address);
        Response<string> CreateUnsignedTransaction(TransactionDTO transaction); 

    }
}