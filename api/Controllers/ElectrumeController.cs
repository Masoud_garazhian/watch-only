using JsonRPC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Controllers
{
    //dotnet publish -c release
    [ApiController]
    // [Route("api/[controller]")]
    [Route("api/[controller]")]
    public class ElectrumController : Base
    {
        private readonly IElectrum electrum;

        public ElectrumController(IElectrum electrum)
        {
            this.electrum = electrum;
        }
        [HttpGet]
        public IActionResult IsOk()
        {
            return Ok("Its Ok!");
        }

        //https://localhost:5003/api/Electrum/GetAddress 
        [HttpGet("GetAddress")]
        public IActionResult GetAddress()
        {
            return this.ResponceHandler(electrum.GetAddress());
        }

        //https://localhost:5003/api/Electrum/GetBalance 
        [HttpGet("GetBalance")]
        public IActionResult GetBalance()
        {
            return this.ResponceHandler(electrum.GetBalance());
        }

        //https://localhost:5003/api/Electrum/GetExAddresses 
        [HttpGet("GetExAddresses")]
        public IActionResult GetExAddresses()
        {
            return this.ResponceHandler(electrum.GetExpandedAddresses());
        }

        //https://localhost:5003/api/Electrum/GetAddresses
        [HttpGet("GetAddresses")]
        public IActionResult GetAddresses()
        {
            return this.ResponceHandler(electrum.GetAddresses());
        }  

        // https://localhost:5003/api/Electrum/BroadcastTransaction
        [HttpPost("BroadcastTransaction")]
        public IActionResult BroadcastTransaction(SignedTx tx)
        {
            return this.ResponceHandler(electrum.BroadcastTransaction(tx));
        }

        //https://localhost:5003/api/Electrum/ValidateAddress
        [HttpGet("ValidateAddress")]
        public IActionResult ValidateAddress(string address)
        {
            return this.ResponceHandler(electrum.ValidateAddress(address));
        }

        //https://localhost:5003/api/Electrum/CreateUnsignedTransaction
        [HttpPost("CreateUnsignedTransaction")]
        public IActionResult CreateUnsignedTransaction(models.TransactionDTO transaction)
        {
            return this.ResponceHandler(electrum.CreateUnsignedTransaction(transaction));
        }
         
    }
}
//https://localhost:5003/swagger/index.html