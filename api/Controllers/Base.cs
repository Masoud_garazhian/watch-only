using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Controllers
{
    public class Base : Controller
    {
        // protected AuthenticateBaseDto TokenUser;

        // public ManageContoller()
        // {
        //     TokenUser = new AuthenticateBaseDto();
        // }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                // TokenUser.MelliId = User.Claims.ToList()[0].Value;
                // TokenUser.MobileNo = User.Claims.ToList()[1].Value;
            }
            catch
            {

            }
            base.OnActionExecuting(context);
        }
        public IActionResult ResponceHandler<T>(Response<T> response) //TODO: log
        {
            try
            {
                if (response.Exception != null)
                    response.HttpStatus = System.Net.HttpStatusCode.InternalServerError;
                if (response.Exception != null || (int)response.HttpStatus == 0)
                    response.HttpStatus = System.Net.HttpStatusCode.OK;
                return StatusCode((int)response.HttpStatus, response);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
                response.HttpStatus = System.Net.HttpStatusCode.InternalServerError;
                return StatusCode((int)response.HttpStatus, response);
            }
            finally
            {
                //TODO: log
            }
        }
    }
}
